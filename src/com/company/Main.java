package com.company;

public class Main {

    public static void main(String[] args) {
        String text = "What'll you do when you get lonely! And no one's waiting by your side.";

        int sentence = text.length() - text.replaceAll("[.!]", "").length();
        int word = text.length() - text.replace(" ", "").length();

        System.out.println("Number of sentences = " + sentence);
        System.out.println("Number of words = " + word);
    }
}